#include <iostream>
#include <float.h>
#include <fstream>
#include <iomanip>

using namespace std;


class Simplex {
private:
    double **matrixA;
    double *vectorB;
    double *vectorC;
    double **simplexTable;
    int rows;
    int cols;
    double *ans;

    void initialize() {
        simplexTable = new double *[rows];
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                simplexTable[i] = new double[cols];
                simplexTable[i][j] = 0;
            }
        }

        for (int i = 0; i < rows - 1; ++i) {
            for (int j = 0; j < cols - 1; ++j) {
                simplexTable[i][j] = matrixA[i][j];
            }
        }
        for (int i = 0; i < rows - 1; ++i) {
            simplexTable[i][cols - 1] = vectorB[i];
        }

        for (int i = 0; i < cols - 1; ++i) {
            simplexTable[rows - 1][i] = -vectorC[i];
        }
    }

    bool isFinish() {
        for (int i = 0; i < cols - 1; ++i) {
            if (simplexTable[rows - 1][i] < 0)
                return false;
        }
        return true;
    }

    int findGeneralColumn() {
        double min = 0.0;
        int index = -1;
        for (int i = 0; i < cols - 1; ++i) {
            if (simplexTable[rows - 1][i] <= min) {
                min = simplexTable[rows - 1][i];
                index = i;
            }
        }
        return index;
    }

    int findGeneralRow(int generalCol) {
        double *tempArray = new double[rows - 1];
        for (int i = 0; i < rows - 1; ++i) {
            if (simplexTable[i][generalCol] > 0) {
                tempArray[i] = simplexTable[i][cols - 1] / simplexTable[i][generalCol];
            } else
                tempArray[i] = DBL_MAX;
        }
        double min = DBL_MAX;
        int minRow = -1;

        for (int i = 0; i < rows - 1; ++i) {
            if (tempArray[i] < min) {
                min = tempArray[i];
                minRow = i;
            }
        }
        return minRow;
    }

    void copy(double** tempTable) {
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                simplexTable[i][j] = tempTable[i][j];
            }
        }
    }

    int isInArray(double* array, int size, int number) {
        for (int i = 0; i < size - 1; ++i) {
            if (array[i] == number)
                return i;
        }
        return -1;
    }

    void buildNewTable() {

        double **newTable;
        newTable = new double *[rows];

        for (int i = 0; i < rows; ++i)
            newTable[i] = new double[cols];
        int generalCol = findGeneralColumn();
        int generalRow = findGeneralRow(generalCol);
        int tempIndex = isInArray(ans, cols, generalRow);
        ans[tempIndex] = -1;

        ans[generalCol] = generalRow;
        double pivot = simplexTable[generalRow][generalCol];
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                if (i == generalRow && j == generalCol) {
                    newTable[i][j] = 1 / pivot;
                    continue;
                }

                if (i == generalRow) {
                    newTable[i][j] = simplexTable[i][j] / pivot;
                    continue;
                }

                if (j == generalCol) {
                    newTable[i][j] = simplexTable[i][j] / (-pivot);
                    continue;
                }

                newTable[i][j] = (pivot * simplexTable[i][j] - simplexTable[generalRow][j]*simplexTable[i][generalCol])/pivot;
            }
        }

        copy(newTable);

        for (int i = 0; i < rows; ++i)
            delete []newTable[i];
        delete []newTable;
    }

public:

    Simplex() {
    }
    ~Simplex() {
        delete[] ans;
        delete []vectorB;
        delete []vectorC;
        for (int i = 0; i < rows - 1; ++i) {
            delete[]matrixA[i];
            delete[]simplexTable[i];
        }
        delete[]matrixA;
        delete[]simplexTable;
    }

    void input() {
        ifstream matrixstream("matrix.txt");
        matrixstream >> rows >> cols;
        matrixA = new double *[rows];
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                matrixA[i] = new double[cols];
                matrixA[i][j] = 0;
            }
        }
        vectorB = new double[rows];
        vectorC = new double[cols];

        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                matrixstream >> matrixA[i][j];
            }
        }

        for (int i = 0; i < rows; ++i) {
            matrixstream >> vectorB[i];
        }

        for (int i = 0; i < cols; ++i) {
            matrixstream >> vectorC[i];
        }
    }

    void simplexMethod() {
        rows++;
        cols++;
        initialize();
        for (int i = 0; i < rows - 1; ++i) {
            delete []matrixA[i];
        }
        delete []matrixA;
        delete []vectorB;
        delete []vectorC;

        ans = new double(cols - 1);
        for (int i = 0; i < cols - 1; ++i) {
            ans[i] = -1;
        }
        while (!isFinish()){
            cout << endl;
            for (int i = 0; i < rows; ++i) {
                for (int j = 0; j < cols; ++j) {
                    cout << setw(3) << simplexTable[i][j] << " ";
                }
                cout << endl;
            }
            buildNewTable();
        }

        cout << endl;
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                cout << simplexTable[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
        for (int i = 0; i < cols - 1; ++i) {
            if (ans[i] != -1) {
                int index = (int)ans[i];
                ans[i] = simplexTable[index][cols - 1];
            } else
                ans[i] = 0.0;
        }
        for (int j = 0; j < cols - 1; ++j) {
            cout << ans[j] << " ";
        }
    }

};


int main() {

    Simplex *simplex = new Simplex();
    simplex->input();
    simplex->simplexMethod();

    return 0;
}